package com.fest.compfestservice.Model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
@Document(collection = "Users")
public class User {
    @Id
    private String id;

    @NotBlank
    @Size(max = 20)
    private String email;

    @NotBlank
    @Size(max = 20)
    private String password;
}


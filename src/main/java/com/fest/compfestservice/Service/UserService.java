package com.fest.compfestservice.Service;

import com.fest.compfestservice.Model.User;
import com.fest.compfestservice.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User findUser(String id) {
        return userRepository.findById(id);
    }
}
